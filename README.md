# Unofficial Poppyseed Pets API

Documents (and, as time goes on, software packages) to describe and interface with the Poppyseed Pets API

At present this consists solely of a markdown document describing
Poppyseed Pets API calls I know about. Eventually, this will be the
basis for a python package to access Poppyseed Pets data and perform
tasks in-game.

# What is Poppyseed Pets?

It's a browser game accessible at <https://poppyseedpets.com>; if you
aren't already playing it, it's unlikely this project has much of
interest to you.