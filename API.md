[[_TOC_]]

# Basics and universal API features

## Required and standard features of API calls

All APIs have a base URL of `https://api.poppyseedpets.com/`, and
almost all use either the `POST` or `GET` request methods; each call
is a suffix appended onto this base URL. Except for a few API calls
noted otherwise (`account/logIn`, `article`, and a few others), every
call requires token authentication, using a token provided on
login. Payloads for `POST` calls should be provided as JSON; many
`GET` calls have query strings. Page numbers on those methods which
take page numbers are indexed starting at 0; a page number past the
end of a search is treated as the last page, typically. Objects and
arrays in query strings are described with bracketed phrases; for
instance, the query-string for a search for creamy, fatty foods would
be
`filter%5Bedible%5D=true&filter%5BfoodFlavors%5D%5B0%5D=Creamy&filter%5BfoodFlavors%5D%5B1%5D=Fatty`,
which is a URL encoding of
`filter[edible]=true&filter[foodFlavors][0]=Creamy&filter[foodFlavors][1]=Fatty`.

## Standard features of API responses

Responses for various API calls are provided as JSON. Responses always
contain a property `success`, with a boolean value. If it is false,
then the property `errors` is an array containing one or more strings
describing the reason for failure. The described responses under
various individual API calls are the contents of the response assuming
`success` is true, i.e., that the call succeeded. Described errors are
potential strings that have been seen as errors. If `success` is `true`,
the response has the following properties:

* `user`: `null` if no player is logged in; a [user](#user-object)
  object describing the logged-in player otherwise.
* `weather`: a [weather](#weather-object) object.
* `data` (optional): A property whose format and meaning varies
  depending on the API call. See specific API calls for details.
* `reloadInventory` (optional): A boolean indicating whether the API
  call caused a substantive change in the player's inventory beyond
  the obvious (e.g. feeding pets, using consumable items, moving
  items, and recycling items do not as a rule have `reloadInventory`
  set true, because the change in inventory is a predictable result of
  the action which an intelligent client would already know about,
  while opening a box does have `reloadInventory` set true, because
  the exact items produced by consuming the box need an inventory
  refresh to be observed). When absent, `reloadInventory` is taken to
  be false.
* `activity` (optional): An array of activities to be reported to the
  player, which are either consequences of the API call or are timed
  events which happened between the prior API call and the current
  one. When absent, no player-reportable activities are presumed to
  have taken place.

The following names are used below to refer to specific types of
strings: a **date** is a string specifying a date and time in ISO 8601
format, always in UTC with a `+00:00` time-offset suffix, and a
**color** is a string of 6 text-coded hexadecimal digits, used to
specify a 24-bit color in the standard "RRGGBB" format.

## Errors relevant to all API calls

* **"No route found for *〈method〉* *〈url〉*"**: received in response to an
 unrecognized API call.
* **"No route found for *〈method〉* *〈url〉*: Method Not Allowed (Allow:
 *〈METHOD〉*)**": received in response to a recognized API call made
 using an invalid method.
* **"You must be logged in to do that. (Curious! Reload the page and try
 again, maybe?)"**: received in response to an API call lacking a
 required token.
* **You have been logged out due to inactivity. Please log in
 again.**: received in response to an API call with an expired token.


* **"Invalid JSON body: Syntax error"**: received when a payload is
  malformed json.
* **"App\\Entity\\*〈objectType〉* object not found by the
  @ParamConverter annotation."**: received when the API's URL itself
  contains a parameter (such as [`pet/`*〈petID〉*](#petpetid), in
  which a pet's ID number is part of the URL, and when that ID is
  unparseable or doesn't refer to an existing element of the
  appropriate database.
  
* **"House hours must be run before you can continue playing."**: received
 when the [`house/runHours`](#houserunhours) API call has not been
 made in a while; this API call must be invoked before any other
 in-game calls can be made.


# API calls

## Logging in and out

### `account/logIn`

Used to log in to the game. This API call does not require an
authorization token.

**Method**: `POST`

**Payload**:

* `email`: The email address associated with the account.
* `passphrase`: The password/passphrase associated with the account
  (unhashed).
* `sessionHours` (optional): The number of hours after which the token
  expires. If `null` or absent, the token uses the default session
  length.

**Response properties**:

* `data`: An object containing at present only the `currentTheme` key,
  which is either `null` if there is no theme or is a
  [theme](#theme-object) object.
* `sessionId`: A token to be used in authenticating all other API calls.

**Errors**:

* **"Email and/or passphrase is not correct."**: received when
  credentials in payload do not match a recognized user.

### `account/logOut`

Used to log out of the game and invalidate the authorization token used.

**Method**: `POST`

**Response properties**: This API call returns no `data` property,
always has a `null` user property, and includes the standard
`weather` property.

### `account/register`

Used to create an account for the game. This API call does not require an
authorization token.

**Method**: `POST`

**Payload**:

* `petColorA`: A color string for the pet's first color.
* `petColorB`: A color string for the pet's second color.
* `petImage`: A string representing the pet's species (and relevant image).
* `petName`: A string representing the pet's name.
* `playerEmail`: A string containing the account's email address.
* `playerName`: A string containing the account's username.
* `playerPassphrase`: A string containing the account's passphrase.
* `theme`: A [theme](#theme-object) object which is the player's
  starting theme.

**Response properties**:

* `sessionId`: A token to be used in authenticating all other API calls.

This API call returns no `data` property, and includes
`reloadInventory` as true.

## Logs and activity reports

## `account`

Used to get the theme (and presumably the user data).

**Method**: `GET`

**Response properties**:

* `data`: An object containing at present only the `currentTheme` key,
  which is either `null` if there is no theme or is a
  [theme](#theme-object) object.

### `house/runHours`

Must be run regularly to process pet activities and other
events. There is a generic error message which is sent if you try to
do another API call when this one hasn't been made recently enough.

**Method**: `POST`

**Response properties**:

* `data`: An object with the single property `pets`, which is an array
  of [pet](#pet-object) objects, containing pet records for each pet in
  your house.

## Pet information and control

### `pet/my`

Lists all pets currently living with the logged-in user.

**Method**: `GET`

**Response properties**:

* `data`: An array of [pet](#pet-object) objects.

### `pet/`*〈petID〉*`/availableMerits`

Determines the merits available to the pet with the given ID
number. Note that this does return a list of merits even for pets not
currently eligible for a merit.

**Method**: `GET`

**Response properties**:

* `data`: An array of objects each of which has the string properties
  `name` and `description`, respectively the short name and detailed
  description of an individual merit.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"*〈name〉* is not your pet."**: received for a pet ID of someone else's pet.

### `pet/`*〈petID〉*

Gets information about the pet of the given ID number.

**Method**: `GET`

**Response properties**:

* `data`: A [pet](#pet-object) object.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.

### `pet/`*〈petID〉*`/chooseAffectionReward/merit`

Chooses an merit for a pet with the given ID number.

**Method**: `POST`

**Payload**:

* `merit`: The name of the merit as a string

**Response properties**:

* `data`: The [pet](#pet-object) object of the pet who took on the expertise.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"*〈name〉* is not your pet."**: received for a pet ID of someone else's pet.
* **"You'll have to raise *〈name〉*'s affection, first."**: received for a pet
  ID of a pet which is not merit-eligible.

### `pet/`*〈petID〉*`/equip/`*〈inventoryID〉*

Makes the pet with the given ID number equip a specific inventory
item, identified by ID number. Note that items can be equipped
directly from the basement or mantel, or even from other pets'
inventory.

**Method**: `POST`

**Response properties**:

* `data`: The [pet](#pet-object) object of the pet who wielded the item.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"App\\Entity\\Inventory object not found by the @ParamConverter
  annotation."**: received for a nonexistent inventory ID number.
* **"There is no such pet."**: received for a pet ID of someone else's pet.
* **"That item does not exist."**: received if the inventory item is the
  possession of some other player.
* **"That item's not an equipment!"**: received if the
  inventory item is not a tool.

### `pet/`*〈petID〉*`/feed`

Feeds objects to a pet.

**Method**: `POST`

**Payload**:

* `items`: An array of inventory IDs for the objects to feed to the
  pet. This can even be empty, but if it is, then the `activity`
  response property gets misleading text claiming the pet is too full
  to eat any more. Foods must be in the house, not in the basement or
  mantel.

**Response properties**:

* `data`: The [pet](#pet-object) object of the pet who was fed.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"At least one of the items selected doesn't seem to exist??"**:
  received when the `items` list isn't in the payload, or when an
  element in it isn't an extant item, or belongs to someone else, or
  is in a non-house location.
* **"You can't feed that pet."**: received for a pet ID of someone else's pet.
* **"At least one of the items selected is not edible!"**:
  received when at least one item in the `items` list is not a food.

### `pet/`*〈petID〉*`/familyTree`

Returns a list of all of the given pet's family.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `children`: An array of [short pet](#short-pet-objects) objects.
  * `grandparents`: An array of [short pet](#short-pet-objects) objects.
  * `parents`: An array of [short pet](#short-pet-objects) objects.
  * `siblings`: An array of [short pet](#short-pet-objects) objects.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.

### `pet/`*〈petID〉*`/friends`

Returns a list of the pet with the given ID number's friends and other
relationships.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties.
  * `friends`: An array of [relationship](#relationship-object)
    objects. This will be an incomplete list; for a full list, use the
    [`pet/`*〈petID〉*`/relationships`](#petpetidrelationships) API
    call.
  * `groups`: An array whose elements are [short
    group](#short-group-object) objects.
  * `guild`: `null` if the pet is not in any guild; otherwise an
    object with the following properties.
    * `guild`: A [short guild](#short-guild-object) object.
    * `joinedOn`: A date string indicating when the pet joined the guild.
    * `rank`: A string describing the pet's rank in the guild.
  * `relationshipCount`: A number describing the number of
    relationships the pet has.
  * `spiritCompanion`: Same as in a [pet](#pet-object) object.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"Generic 403333333!!1! Reload and try again; if the problem
  persists, let Ben know, so he can fix it! :P"**: received for a
  pet ID of someone else's pet.

### `pet/`*〈petID〉*`/logs`

Returns a log of all of the pet with the given ID number's activities.

**Method**: `GET`

**Query string**:

* `page` (optional): The number of the page in the log to fetch; zero
  by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  [activity](#activity-object) objects.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"Generic 403333333!!1! Reload and try again; if the problem
  persists, let Ben know, so he can fix it! :P"**: receiveded for a
  pet ID of someone else's pet.

### `pet/`*〈petID〉*`/pet`

Comforts the pet with the given ID number, increasing their safety and
esteem.

**Method**: `POST`

**Response properties**:

* `data`: The [pet](#pet-object) object of the pet who got petted.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"You can't pet that pet."**: received for a pet ID of someone else's pet.
* **"You've already interacted with this pet recently."**: received for a pet
  ID of a pet owned by you which has recently been petted.

### `pet/`*〈petID〉*`/pickExpertise`

Chooses an expertise for the pet with the given ID number.

**Method**: `POST`

**Payload**:

* `expertise`: The string "Force of Will", "Force of Nature", or "Balance".

**Response properties**:

* `data`: The [pet](#pet-object) object of the pet who took on the expertise.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"That's not your pet."**: received for a pet ID of someone else's pet.
* **"This pet is not ready to have a talent picked."**: received for a
  pet ID of a pet owned by you which is not expertise-eligible.

### `pet/`*〈petID〉*`/putInLunchbox/`*〈inventoryID〉*

Puts a specific inventory item, identified by ID number into the
lunchbox of a pet with a particular ID number. Note that items can be
placed directly into pets' lunchboxes from the basement or mantel.

**Method**: `POST`

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"App\\Entity\\Inventory object not found by the @ParamConverter
  annotation."**: received for a nonexistent inventory ID number.
* **"There is no such pet."**: received for a pet ID of someone else's pet.
* **"That item does not exist."**: received if the inventory item is the
  possession of some other player.
* **"Only foods can be placed into lunchboxes."**: received if the
  inventory item is not a food.
* **"That item is in *〈name〉*'s lunchbox."**: received if the inventory
  item is already in a lunchbox.

### `pet/`*〈petID〉*`/relationships`

Returns a list of all of the given pet's relationships.

**Method**: `GET`

**Query string**:

* `page` (optional): The number of the page in the list of
  relationships to fetch; zero by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  [relationship](#relationship-object) objects.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.

### `pet/search`

Searches for pets in the whole universe.

**Query string**:

* `filter` (optional): An object (see [query-string
  construction](#required-and-standard-features-of-api-calls)) with
  any or all of the following properties:
  * `name`: A string to filter against as a partial or exact name for pets.
  * `species`: A string to filter against as a partial or exact name for pet species.
* `page` (optional): The number of the page in the inventory search to fetch;
  zero by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  [pet](#pet-object) objects.

### `pet/`*〈petID〉*`/setFertility`

Determines whether the pet of the given ID number can become pregnant.

**Method**: `PATCH`

**Payload**:

* `fertility`: A boolean denoting whether the pet can become pregnant.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"*〈name〉* is not your pet."**: received for a pet ID of someone else's pet.
* **"*〈name〉* does not have the Volagamy merit."**: received when the pet
    is not able to be preganant.

### `pet/`*〈petID〉*`/stopHelping`

Retrieves the pet with the given ID from one of the locations where a
pet can be assigned as a helper. See also the API calls
[`greenhouse/assignHelper/`*〈petID〉*](#greenhouseassignhelperpetid),
[`beehive/assignHelper/`*〈petID〉*](#beehiveassignhelperpetid), and
[`dragon/assignHelper/`*〈petID〉*](#dragonassignhelperpetid).

**Method**: `POST`

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"That pet is not currently helping out anywhere..."**: received if
  the pet referred to is not assigned as a helper.
* **"That pet does not exist, or does not belong to you."**: received
  for a pet ID of someone else's pet.

### `pet/`*〈petID〉*`/updateNote`

Sets the private note field for the pet of the given ID number.

**Method**: `POST`

**Payload**:

* `note`: A string of text to overwrite the current note.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID number.
* **"There is no such pet."**: received for a pet ID of someone else's pet.

### `pet/typeahead`

Returns a list of owned pets whose names match the given substring;
used by the front end for selecting pets as helpers, etc.

**Method**: `GET`

**Query string**:

* `search`: The string to look for in a pet's name.

**Response properties**:

* `data`: An array of [pet](#pet-object) objects.

### `petGroup/`*〈groupID〉*

Gets information about the pet group of the given ID number.

**Method**: `GET`

**Response properties**:

* `data`: A [group](#group-object) object.

**Errors**:

* **"App\\Entity\\PetGroup object not found by the @ParamConverter
  annotation."**: received for a nonexistent group ID number.

## Inventory information and control

### `inventory/moveTo/`*〈location〉*

Moves items to a different inventory location, specified numerically
(0=house, 1=basement, 2=mantel).

**Method**: `POST`

**Payload**:

* `inventory`: An array of inventory IDs for items to move.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"Some of the items could not be found??"**: received when the
  inventory list isn't provided at all, or when an element in it isn't
  an extant inventory item, or belongs to someone else.
* **"You do not have enough space in your house!"**: received when the
  destination is the house and there is not enough room to move the
  items listed.
* **"The mantle only has space for 24 items."**: received when the
  destination is the mantle and there is not enough room to move the
  items listed.
* **""Invalid location given."**: received when the destination is not
  a valid location number. Note that before acquiring a basement or
  fireplace, locations 1 or 2 respectively will be invalid.


### `inventory/my`

Lists all items currently in the logged-in user's house.

**Method**: `GET`

**Response properties**:

* `data`: An array of [inventory](#inventory-object) objects.
* `user`, `weather`: As for every API call.

### `inventory/my/`*〈location〉*

Searches all items currently the logged-in user possesses in the
location denoted by the given numeric identifier (0=everywhere,
1=basement, 2=mantle, 3=pet possessions, 4=somewhere but seems to be
empty (TODO: what is location 4?)).

**Method**: `GET`

**Query string**:

* `filter` (optional): An object (see [query-string
  construction](#required-and-standard-features-of-api-calls)) with
  any or all of the following properties:
  * `aHat`: A boolean denoting whether to filter for or against hats.
  * `bonus`: A boolean denoting whether to filter for or against tool
   bonus providers.
  * `edible`: A boolean denoting whether to filter for or against
    food.
  * `equipable`: A boolean denoting whether to filter for or against
    tools.
  * `equipstats`: An array of strings denoting required stats effects
    for tools. Valid values include "brawl", "nature", "fishing",
    "stealth", "umbra", "science", "gathering", "climbing", "crafts",
    "music", "smithing".
  * `foodFlavors`: An array of strings denoting required flavors for
    foods. Valid values include "Earthy", "Spicy", "Planty", "Fatty",
    "Fruity", "Creamy", "Fishy", "Oniony", "Tannic", "Meaty",
    "Floral", and "Chemically".
  * `hasDonated`: A boolean denoting whether to filter for or against
    donated items.
  * `name`: A string to filter against as a partial or exact name for items.
  * `nameExactMatch`: A boolean to determine whether name matches need
    to be exact.
  * `spice`: A boolean denoting whether to filter for or against spices.
* `page` (optional): The number of the page in the inventory search to fetch;
  zero by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  [inventory](#inventory-object) objects.

**Errors**:

* **"Invalid location given."**: number given does not correspond to any
  known location.


### `inventory/prepare`

Combines or cooks items.

**Method**: `POST`

**Payload**:

* `inventory`: An array of inventory IDs for items to combine.

**Response properties**:

* `data`: An array of [inventory](#inventory-object) objects produced
  by the combination/cooking of the given objects.

**Errors**:

* **"You gotta' select at least ONE item!"**: received when the
  inventory list is present but empty.
* **"Some of the items could not be found??"**: received when the
  inventory list isn't provided at all, or when an element in it isn't
  an extant inventory item, or belongs to someone else.
* **"You can't make anything with those ingredients."**: received when
  the items in question can't be combined.
* **"All of the items must be in the same location."**: received when
  the inventory list contains items in multiple different locations.

### `inventory/sell`

Sets or removes a price on items to be sold in the market.

**Method**: `POST`

**Payload**:

* `items`: An array of inventory IDs for items to be sold.
* `price` (optional): A single number representing the sell price
  being placed on every individual item; setting the price to `0` or
  leaving the price out of the payload corresponds to taking the item
  off the market. Negative values are treated like zero; noninteger
  values are rounded down.

**Response properties**:

* `data` (optional): A number representing the sale price, if selling;
  absent if taking items off the market.

**Errors**:

* **"You must select at least one item!"**: received when the `items`
  list is absent or empty.
* **"One or more of the selected items do not exist! Maybe reload and
  try again??"**: received when an element in the inventory list isn't
  an extant inventory item, or belongs to someone else.
* **"You cannot list items for more than *〈maxPrice〉* moneys. See the
  Market Manager to see if you can increase this limit!"**: received
  when the requested `price` exceeds the player's sell-price limit.

### `inventory/throwAway`

Recycles items.

**Method**: `POST`

**Payload**:

* `inventory`: An array of inventory IDs for items to recycle.

**Response properties**:

* `data`: An empty array, for some reason.

**Errors**:

* **"Some of the items could not be found??"**: received when the
  inventory list isn't provided at all, or when an element in it isn't
  an extant inventory item, or belongs to someone else.

### `item/`*〈itemtype〉*`/`*〈inventoryID〉*`/`*〈action〉*

Items with actions have elements of the `useActions` property whose
second element is a string of the form
"*〈itemtype〉*/#/*〈action〉*"; e.g. the Bag of Beans item has
`useActions[0][1]` value "box/bagOfBeans/#/open". These actions are
invoked by an API call where the "#" is replaced with an inventory
object's `id` value; e.g. a bag of beans with `id` 5274495 is opened
with the API call `item/box/bagOfBeans/5274495/open`.

**Method**: `POST`

**Response properties**:

* `data`: An object with the two following properties:
  * `itemDeleted` (optional): A boolean, usually true when present,
    indicating whether the inventory object being used is removed from
    the inventory.
  * `text`: String describing the action in user-readable form.

**Errors**:

* **"App\\Entity\\Inventory object not found by the @ParamConverter
  annotation."**: received for a nonexistent inventory ID number.
* **"That item no longer exists, or cannot be used in that way."**:
  received when the specific action/itemtype template being invoked
  isn't valid for the inventory item, or when the inventory item
  belongs to someone else.

## Greenhouse management

### `greenhouse`

Used to get a full report of the greenhouse's status.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `fertilizer`: An array describing items in the player's
    possession, with each entry being an object with the following
    limited [inventory](#inventory-object) object-like structure:
    * `enchantment`: As in an [inventory](#inventory-object) object.
    * `id`: As in an [inventory](#inventory-object) object.
    * `item`: An object with the following properties:
	  * `fertilizerRating`: A number representing the item's efficacy
        as a fertiilizer (1=F, 2=D, 3=C, 4=B, 5=A, 6=A+, 10=★★).
	  * `image`: As in an [item](#item-object) object.
	  * `name`: As in an [item](#item-object) object.
  * `greenhouse`: An object with the following properties:
    * `hasBirdBath`: A boolean indicating whether there is a bird
      bath.
    * `hasComposter`: A boolean indicating whether there is a
      composter.
    * `helper`: `null` if there is no garden helper; otherwise a
      [short pet](#short-pet-object) object
	* `maxDarkPlants`: The number of dark plots in the greenhouse.
	* `maxPlants`: The number of earth plots in the greenhouse.
	* `maxWaterPlants`: The number of water plots in the greenhouse.
    * `visitingBird`: `null` when no bird is present; a text string
      indicating the variety of bird if one is visiting the bird bath.
  * `plants`: An array of objects with the following properties:
    * `canNextInteract`: A date string describing the next time when
      the plant can be fertilized or harvested.
	* `id`: An number which is an internal ID code for the plant.
	* `image`: A string describing the location of the plant's art asset.
	* `isAdult`: A boolean, whose purpose doesn't seeem clear; it's
      `true` on all my plants exceopt a school of fish (TODO: figure
      it out).
	* `ordinal`: A nubmer which seems to serve as some sort of
      internal counter, perhaps of how many plants the player planted
      before this one?
    * `plant`: An object with the following properties:
	  * `type`: A string describing the plot type ("dark", "earth", or
        "water").
	  * `name`: The name of the plant itself as displayed on screen.
	* `pollinators`: `null` or a string describing pollinators for
      this plant ("bees1", "bees2", and "butterflies" have been seen).
    * `previousProgress`: A numeric value between 0 and 1 indicating
      growth progress prior to the most recent fertilization.
    * `progress`: A numeric value between 0 and 1 indicating current
      growth progress.
  * `weeds`: `null` or a string used to populate the weeding button
    (e.g. "Destroy all weeds!").

**Errors**:

* **"You haven't purchased a Greenhouse plot yet."**: received if the
  user has not unlocked the greenhouse.

### `greenhouse/`*〈plantID〉*`/fertilize`

Used to fertilize the plot with the given ID number. The game will
allow fertilization of plots ready for harvest, but doing so confers
no benefit and will then reset the "interaction" timer so that harvest
can't take place until later.

**Method**: `POST`

**Payload**:

* `fertilizer`: A number denoting the inventory ID of the item to be
  used as fertilizer.

**Response properties**: The `data` property is identical to the
responses of the [`greenhouse`](#greenhouse) call, depicting new
greenhouse state.

**Errors**:

* **"App\\Entity\\GreenhousePlant object not found by the
  @ParamConverter annotation."**: received if the plant ID does not
  correspond to an entry in the plant database.
* **"This plant is not yet ready to fertilize."**: received if the
  plant ID is of a plant which has been fertilized recently.
* **"A fertilizer must be selected."**: received if the `fertilizer`
  field of the payload is absent, invalid, not actually a fertilizer,
  or corresponds to an object not in the player's possession (basement
  and mantle are OK).

### `greenhouse/`*〈plantID〉*`/harvest`

Used to harvest the plot with the given ID number.

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`greenhouse`](#greenhouse) call, depicting new
greenhouse state. The `reloadInventory` property is likely to be true
unless something weird happened.

**Errors**:

* **"App\\Entity\\GreenhousePlant object not found by the
  @ParamConverter annotation."**: received if the plant ID does not
  correspond to an entry in the plant database.
* **"That plant does not exist."**: received if the plant to be
  harvested is someone else's or a once-extant plant still in the
  database.
* **"This plant is not yet ready to harvest."**: received if the plant
  to be harvested is not fully mature.
* **"Magic Beanstalk cannot be harvested!"**: received if the plant to
  be harvested is a Magic Beanstalk.

### `greenhouse/`*〈plantID〉*`/pullUp`

Used to pull up the plot with the given ID number.

**Method**: `POST`

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"App\\Entity\\GreenhousePlant object not found by the
  @ParamConverter annotation."**: received if the plant ID does not
  correspond to an entry in the plant database.

### `greenhouse/assignHelper/`*〈petID〉*

Used to assign the pet of the given ID number as your greenhouse
helper. See also the API call
[`pet/`*〈petID〉*`/stopHelping`](#petpetidstophelping).

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`greenhouse`](#greenhouse) call, depicting new
greenhouse state. 

**Errors**:

* **"You haven't got a Greenhouse, yet!"**: received if the
  user has not unlocked the greenhouse.
* **"Your Greenhouse already has a helper! *〈name〉*!"**: received
  if the greenhouse already has a helper.
* **"That pet does not exist, or does not belong to you."**: received
  for a pet ID of someone else's pet.

### `greenhouse/composter/feed`

Used to add items to the composter

**Method**: `POST`

**Payload**:

* `food`: An array of inventory IDs to be fed to the composter.

**Response properties**: The `data` property is identical to the
responses of the [`greenhouse`](#greenhouse) call, depicting new
greenhouse state. 

**Errors**:

* **"You haven't purchased a Greenhouse plot yet."**: received if the
  user has not unlocked the greenhouse.
* **"No items were selected as fuel???"**: received if the `food`
  field of the payload is missing.
* **"Some of the compost items selected could not be used. That
  shouldn't happen. Reload and try again, maybe?"**: received if the
  `food` field of the payload is not an array, or if it contains
  noncompostable items, invalid IDs, or items not in the house.
* **"That would leave you with *〈number〉* items at home. (100 is the
  usual limit.)"**: received if the composter's output is larger than
  its input and the player's house is very full.
  
### `greenhouse/plantSeed`

Used to plant a new seed.

**Method**: `POST`

**Payload**:

* `seed`: A number denoting the inventory ID of the seed to be planted.

**Response properties**: The `data` property is identical to the
responses of the [`greenhouse`](#greenhouse) call, depicting new
greenhouse state. 

**Errors**:

* **"You don't have a greenhouse!"**: received if the user has not
  unlocked the greenhouse.
* **""seed" is missing, or invalid."**: received if the `seed` field
  of the payload is missing or non-numeric.
* **"There is no such seed. That's super-weird. Can you reload and try
  again?"**: received if the seed is the inventory ID of an object
  that can't be used as a seed, or an object belonging to someone
  else, or a number that doesn't resolve to an inventory item.
* **"You can't plant anymore plants of this type."**: received if the
  greenhouse already has as many plants of the seed's type as are
  allowed.

### `greenhouse/seeds/`*〈location〉*

Used to get a list of available seeds for planting in the given
location. Location is (unlike most URL-path parameters) a string, one
of "earth", "water", or "dark". An interesting footnote: you do not
need a greenhouse to make this API call.

**Method**: `GET`

**Response properties**:

* `data`: An array of very minimalistic [inventory](#inventory-object)
  object-like structure:
  * `id`: As in an [inventory](#inventory-object) object.
  * `item`: An object with the following properties:
    * `image`: As in an [item](#item-object) object.
    * `name`: As in an [item](#item-object) object.

**Errors**:

* **"Must provide a valid seed type ("earth", "water", etc...)"**:
  received when the location provided is not a valid location.

### `greenhouse/updatePlantOrder`

Used to reorder plants

**Method**: `POST`

**Payload**:

* `order`: An array of plant IDs, in the order to be rearranged into

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"You don't have a greenhouse!"**: received if the user has not
  unlocked the greenhouse.
* **"Must provide a list of plant ids, in the order you wish to save
  them in."**: received if the `order` field of the payload is missing
  or invalid.
* **"A nasty error occurred! Don't worry: Ben has been
  e-mailed... he'll get it sorted. In the meanwhile, just try
  reloading and trying again!"**: received if the `order` field of the
  payload is an array with non-numeric entries.
* **"The list of plants must include the full list of your plants; no
  more; no less!"**: received if the `order` field of the payload is
  not a full list of plants.

### `greenhouse/weed`

Used to weed the greenhouse.

**Method**: `POST`

**Response properties**:

* `data`: A string briefly describing the result of weeding.

**Errors**:

* **"You don't have a Greenhouse plot."**: received if the user has
  not unlocked the greenhouse.
* **"Your garden's doin' just fine right now, weed-wise."**: received
  if the greenhouse has no weeds.

## Beehive management

### `beehive`

Used to get a full report of the beehive's status.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `alternateRequestedItem`: An object with a limited
    [item](#item-object) object-like structure:
    * `hat`: As in an [item](#item-object) object.
    * `image`: As in an [item](#item-object) object.
    * `name`: As in an [item](#item-object) object.
    * `nameWithArticle`: A string similar to the `name` property, but
      with an article.
    * `tool`: As in an [item](#item-object) object.
  * `flowerPower`: A number representing how long the hive's
    requested-item-driven productivity will last, in hours
  * `helper`: `null` if there is no beehive helper; otherwise a [short
	pet](#short-pet-object) object.
  * `honeycombPercent`: A number between 0 and 1 indicating current
    progress towards producing a honeycomb.
  * `miscPercent`: A number between 0 and 1 indicating current
    progress towards producing "normal bee stuff".
  * `queenName`: A string representing the name of the hive's queen.
  * `requestedItem` An object with the same structure as the
    `alternateRequestedItem` property.
  * `royalJellyPercent`: A number between 0 and 1 indicating current
    progress towards producing royal jelly.
  * `workers`: A number between 0 and 1 representing the nubmer of
    workers in the hive.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.

### `beehive/assignHelper/`*〈petID〉*

Used to assign the pet of the given ID number as your beehive
helper. See also the API call
[`pet/`*〈petID〉*`/stopHelping`](#petpetidstophelping).

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`beehive`](#beehive) call, depicting new beehive
state.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.
* **"Your beehive already has a helper! *〈name〉*!"**: received
  if the beehive already has a helper.
* **"That pet does not exist, or does not belong to you."**: received
  for a pet ID of someone else's pet.

### `beehive/dice`

Used to get a list of dice available to roll to change the hive's needs.

**Method**: `GET`

**Response properties**:

* `data`: An array of [inventory](#inventory-object) objects.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.

### `beehive/assignHelper/`*〈petID〉*

Used to assign the pet of the given ID number as your beehive
helper. See also the API call
[`pet/`*〈petID〉*`/stopHelping`](#petpetidstophelping).

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`beehive`](#beehive) call, depicting new beehive
state.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.

### `beehive/feed`

Used to give the beehive one of their two requested items.

**Method**: `POST`

**Payload**:

* `alternate`: A boolean indicating whether the hive is being fed
  their primary or alternate requested item. If absent, treated as
  `false`.

**Response properties**: The `data` property is identical to the
responses of the [`beehive`](#beehive) call, depicting new beehive
state.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.
* **"You do not have *〈item〉* in your house."**: received if
  the requested item is not in your house.

* **"The colony is still working on the last item you gave them."**:
  received if productivity is still boosted from a previous donation.

### `beehive/harvest`

Used to harvest all completed beehive products. Can succeed even if no
beehive processes are available, and produces a spurious activity-log
line.

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`beehive`](#beehive) call, depicting new beehive
state.

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.

### `beehive/reRoll`

Used to roll a die to change the beehive's desired items.

**Method**: `POST`

**Payload**:

* `die`: A number representing the inventory ID of the die to roll.

**Response properties**: The `data` property is identical to the
responses of the [`beehive`](#beehive) call, depicting new beehive
state.

**Errors**:

**Errors**:

* **"You haven't got a Beehive, yet!"**: received if the user has not
  unlocked the beehive.
* **"A die must be selected!"**: received if the `die` field of the
  payload is absent or non-numeric.
* **"The selected item does not exist! (Reload and try again?)"**:
  received if the selected inventory ID is nonexistent (or possibly
  corresponds to an object belonging to someone else).
* **The selected item is not a die! (Reload and try again?)"**:
  received if the selected inventory ID is for a non-die.

## Dragon den management

### `dragon`

Used to get a full report of the dragon den's status.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `appearance`: A number which presumably dictates how the dragon looks?
  * `colorA`: A color string for the dragon's primary color.
  * `colorA`: A color string for the dragon's secondary color.
  * `gems`: A number representing the number of gems the dragon has
    been given.
  * `gold`: A number representing the number of gold ingots the dragon
    has been given.
  * `greetings`: A string containing the text appearing in the
    dragon's speech bubble.
  * `helper`: `null` if there is no dragon estate manager; otherwise a
    [short pet](#short-pet-object) object.
  * `hostage`: `null` for my dragon (TODO: find its use)
  * `name`: A string containing the dragon's name.
  * `silver`: A number representing the number of silvere ingots the
    dragon has been given.
  * `thanks`: An array of strings which are displayed (not sure how
    one is chosen) when an offering is made.

**Errors**:

* **"You don't have an adult dragon!"**: received if the user has not
  unlocked the dragon den.

### `dragon/assignHelper/`*〈petID〉*

Used to assign the pet of the given ID number as your dragon's estate
manager. See also the API call
[`pet/`*〈petID〉*`/stopHelping`](#petpetidstophelping).

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`dragon`](#dragon) call, depicting new
dragon-den state. 

**Errors**:

* **"You haven't got a Dragon Den, yet!"**: received if the user has not
  unlocked the dragon den.
* **"Your Dragon Den already has a helper! *〈name〉*!"**: received
  if the dragon den already has a helper.
* **"That pet does not exist, or does not belong to you."**: received
  for a pet ID of someone else's pet.

### `dragon/giveTreasure`

Used to give treasures to the dragon. An empty list will return
success but have no real effect.

**Method**: `POST`

**Payload**:

* `treasure`: An array of inventory IDs to be given to the dragon.

**Response properties**: The `data` property is identical to the
responses of the [`dragon`](#dragon) call, depicting new
dragon-den state. 

**Errors**:

* **"No items were selected to give???"**: received if the `treasure` field
  of the payload is missing.
* **"You don't have an adult dragon!"**: received if the user has not
  unlocked the dragon den.
* **"Some of the treasures selected... maybe don't exist!? That
  shouldn't happen. Reload and try again."**: received if the
  `treasure` field of the payload is not an array, or if it contains
  nontreasures, invalid IDs, or items not in the house.

### `dragon/offerings`

Used to get a full report of the dragon den's status.

**Method**: `GET`

**Response properties**:

* `data`: An array of very minimalistic [inventory](#inventory-object)
  object-like structure:
  * `enchantment`: As in an [inventory](#inventory-object) object.
  * `id`: As in an [inventory](#inventory-object) object.
  * `item`: An object with the following properties:
    * `image`: As in an [item](#item-object) object.
    * `name`: As in an [item](#item-object) object.
    * `treasure`: An object with three properties to indicate the
      item's value as an offering:
	  * `gems`: A number representing the gem value of the item.
	  * `gold`: A number representing the gold value of the item.
	  * `silver`: A number representing the silver value of the item.
  * `sellPrice`: As in an [inventory](#inventory-object) object.
  * `spice`: As in an [inventory](#inventory-object) object.

**Errors**:

* **"You don't have an adult dragon!"**: received if the user has not
  unlocked the dragon den.

## Hollow earth management

### `hollowEarth`

Used to get a full report of the hollow earth's status.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `dice`: An array of objects with the following properties:
    * `image`: As in an [item](#item-object) object.
    * `item`: A string containing the name of the die.
	* `quantity`: A number indicating the quantity of dice of this
      type available.
	* `size`: A number indicating the highest value this die can roll.
  * `map`: An array of objects, each with the following properties:
    * `availableGoods`: An array of strings, empty for tiles which
      don't produce goods, and a list of goods possibly produced there
      for tiles which do.
	* `fixed`: A boolean indicating whether the tile cannot be replaced
      or moved.
    * `goodsSide`: `null` if the tile does not produce goods; a string
      representing a compass point ("N", "E", "W", or "S") if the tile
      produces goods.
	* `id`: A number, apparently one more than the array index but
      possibly with internal significance.
    * `image`: `null` if a tile is blank, the empty string for a boxed
      question mark,a nd otherwise a string indicating the location of
      an art asset for the tile.
    * `isTradingDepot`: A boolean indicating whether a tile engages
      with players passing it (e.g. the Jeweler's Workshop).
	* `name`: A string containing the name of the tile.
    * `selectedGoods`: `null` on tiles without assigned goods and a
      string representing the goods type for those with assigned goods..
	* `types`: An array of strings which represent the types of tiles
      which can be legally placed on the space.
	* `x`: A number representing the horizontal position (starting at
      0) of the tile in the hollow earth map-grid.
	* `y`: A number representing the vertical position (starting at
      0) of the tile in the hollow earth map-grid.
  * `player`: An object with the following properties:
    * `action`: `null` when there is no recent result to report to a
      player, but to report the result of landing in a square, this is
      an object with the following properties:
      * `amount` (if `type` is "payMoneys?"): A number of moneys to be
        electively spent at this point.
      * `buttons` (if `type` is "chooseOne"): An array of a single
        string containing the text to print on the confirmation
        button.
	  * `buttonText` (if `type` is "onward" or missing): A string containing the
        text to print on the acknowledgment button.
	  * `description`: A string containing the text to be shown to the
        user describing the action which was taken. This string may
        contain substitutions, e.g. the substring "%pet.name%" to be
        expanded into the name of the pet.
      * `item` (if `type` is "payItem?"): A string containing the item
        which can be used and consumed at this point.
      * `type` (optional): A string indicating the type of action.
	* `amber`: A number, currently zero for me (related to goods-trading?)
	* `chosenPet`: A [short pet](#short-pet-object) object describing
      the current adventurer.
	* `currentTile`: An object with the following properties:
      * `goodsSide`: `null` if the player is not on a goods-producting
        tile; a string representing a compass point if the tile
        produces goods.
      * `moveDirection`: A string representing the direction of the
        next move as a compass point ("N", "E", "W", or "S").
      * `x`: A number representing the horizontal position (starting
        at 0) of the player in the hollow earth map-grid.
	  * `y`: A number representing the vertical position (starting at
        0) of the player in the hollow earth map-grid.
	* `fruit`: A number, currently zero for me (related to goods-trading?)
	* `incense`: A number, currently zero for me (related to goods-trading?)
	* `jade`: A number, currently zero for me (related to goods-trading?)
	* `movesRemaining`: A numberindicating how much of a die-roll is
      still available to move the adventurer.
	* `salt`: A number, currently zero for me (related to goods-trading?)
	* `showGoods`: A boolean, currently false for me (related to goods-trading?)

### `hollowEarth/changePet/`*〈petID〉*

Used to assign the pet of the given ID number as your hollow earth
adventurer.

**Method**: `POST`

**Response properties**: The `data` property is identical to the
responses of the [`hollowEarth`](#hollowearth) call, depicting new
Hollow Earth state. 

**Errors**:

* **"Generic 403333333!!1! Reload and try again; if the problem
  persists, let Ben know, so he can fix it! :P"**: received for a pet
  ID of someone else's pet.

### `hollowEarth/changeTileGoods`

Used to change the goods made available on the current tile.

**Method**: `POST`

**Payload**:

* `goods` (optional): A string representing the goods chosen.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"This tile is not capable of producing that type of good."**:
  received if the `goods` field of the payload is absent, invalid, or
  not a goods type available at the current location.

### `hollowEarth/continue`

Used to acknowledge or choose an action and continue onwards through
the Hollow Earth.

**Method**: `POST`

**Payload**:

* `choice` (optional): A number representing which button was chosen
  by the player in response to the last action.
* `payUp` (optional): A boolean representing whether a payment was
  made in response to the last action.

**Response properties**: The `data` property is identical to the
responses of the [`hollowEarth`](#hollowearth) call, depicting new
Hollow Earth state. In particular, the `player.action` and
`player.movesRemaining` properties may contain interesting details.

**Errors**:

* **"No moves remaining! Roll a die to continue moving."**: received
  if the player is not in the middle of a move.

### `hollowEarth/myTiles`

Used to get a list of tiles which match a certain terrain.

**Method**: `GET`

**Query string**:

* `types` : An array (see [query-string
  construction](#required-and-standard-features-of-api-calls)) of
  strings, which are terain types to match in the tiles.
  
**Response properties**:

* `data`: An array of very minimalistic [inventory](#inventory-object)
  object-like structure:
  * `id`: As in an [inventory](#inventory-object) object.
  * `item`: An object with the following properties:
    * `image`: As in an [item](#item-object) object.
    * `name`: As in an [item](#item-object) object.
  * `sellPrice`: As in an [inventory](#inventory-object) object.

**Errors**:

* **"The types of tiles is missing."**: received if the query string
  has no `types[]` field.

### `hollowEarth/roll`

Used to spend a die to move an adventurer through the Hollow Earth.

**Method**: `POST`

**Payload**:

* `die`: A string which is the item name of the type of die to roll.

**Response properties**: The `data` property is identical to the
responses of the [`hollowEarth`](#hollowearth) call, depicting new
Hollow Earth state. In particular, the `player.action` and
`player.movesRemaining` properties may contain interesting details.

**Errors**:

* **"You must specify a die to roll."**: received if the `die` field
  of the payload is missing, invalid, or not descriptive of a die.
* **"You do not have a *〈item〉*!"**: received if the `die` field
  describes a valid die which is not help by the player.
* **"Cannot roll a die at this time..."**: received if in the middle
  of a roll.

### `hollowEarth/setTileCard`

Used to place a tile in a particular space.

**Method**: `POST`

**Payload**:

* `item`: A number representing the inventory ID of a tile to place.
* `tile`: A number which is the ID number (as reported in the `map`
  property) of the tile position on the Hollow Earth map.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"That space in the Hollow Earth does not exist?!?! (Maybe reload
  and try again...)"**: received if `tile` is missing or invalid.
* **"That item couldn't be found! (Reload and try again.)"**: received
  if `item` is invalid.
* **"That item isn't a Hollow Earth Tile! (Weird! Reload and try
  again...)"**: received if the selected item is an object in the
  house which is not a Hollow Earth tile.
* **"You can't use that Tile on this space! (The types don't
  match!)"**: received if the selected item is a tile of the wrong
  type for the map space selected.
* **"You already have that Tile on the map! (Each Tile can only appear
  once!)"**: received if the selected item is a tile which is already
  on the map.

## Fireplace management

### `fireplace`

Used to get a full report of the fireplace's status.

**Method**: `GET`

**Response properties**:

* `data`: An object with the following properties:
  * `fireplace`: An object with the following properties:
    * `currentStreak`: A number indicating the length (in minutes)
      that your fireplace has been lit on its most recent ignition.
    * `hasReward`: A boolean indicating whether there is a reward
      ready to be taken out of the fire.
	* `heatDescription`: `null` if there is no fire; otherwise a
      descriptive string.
	* `helper`: `null` if there is no chimney sweep; otherwise a
	  [short pet](#short-pet-object) object.	  
    * `longestStreak`: A number indicating the length (in minutes)
      that your fireplace was lit on its longest ignition.
	* `mantleSize`: A number indicating how many items can be put on
      the mantle.
	* `stocking`: An object describing the fireplace stocking which
      appears in the Stocking Stuffing Season, with the following properties;
	  * `appearance`: A string describing the stocking appearance
        (choices are "tasseled", "fluffed", and "snowflaked").
	  * `colorA`: A color string denoting the stocking's first color.
	  * `colorB`: A color string denoting the stocking's second color.
  * `mantle`: An array of [inventory](#inventory-object) objects,
    containing inventory records for every item on the mantle.
  * `whelp`: `null` if there is have no magma whelp; otherwise
    presubably some display information.

### `fireplace/assignHelper/`*〈petID〉*

Used to assign the pet of the given ID number as your chimney
sweep. See also the API call
[`pet/`*〈petID〉*`/stopHelping`](#petpetidstophelping).

**Method**: `POST`

**Response properties**: The `data` property is identical to the
`data[fireplace]` rproperty of the [`fireplace`](#fireplace) call,
depicting new fireplace state.

**Errors**:

* **"You haven't got a Fireplace, yet!"**: received if the user has not
  unlocked the beehive.
* **"Your Fireplace already has a helper! *〈name〉*!"**: received
  if the beehive already has a helper.
* **"That pet does not exist, or does not belong to you."**: received
  for a pet ID of someone else's pet.

### `fireplace/feed`

Used to feed the fire in the fireplace.

**Method**: `POST`

**Payload**:

* `fuel`: An array of inventory IDs (represented as strings, not
  numbers) of objects to set on fire.

**Response properties**: The `data` property is identical to the
`data[fireplace]` rproperty of the [`fireplace`](#fireplace) call,
depicting new fireplace state.

**Errors**:

* **"No items were selected as fuel???"**: received if `fuel` is missing.
* **"Some of the fuel items selected could not be used. That
  shouldn\u0027t happen. Reload and try again, maybe?"**: received if
  `fuel` is not an array, contains invalid IDs, contains items not in
  the house, or contains non-fuel items.

### `fireplace/fuel`

Used to get a list of usable fireplace fuels.

**Method**: `GET`

**Response properties**:

* `data`: An array describing items in the player's
  possession, with each entry being an object with the following
  limited [inventory](#inventory-object) object-like structure:
    * `enchantment`: As in an [inventory](#inventory-object) object.
    * `id`: As in an [inventory](#inventory-object) object.
    * `item`: An object with the following properties:
	  * `fuelRating`: A number representing the item's efficacy
        as a fertiilizer (0=F, 1=D, 2=C, 3=B, 4=A, 5=A+, 6=S, 7=S+).
	  * `image`: As in an [item](#item-object) object.
	  * `name`: As in an [item](#item-object) object.
    * `sellprice`: As in an [inventory](#inventory-object) object.
    * `spice`: As in an [inventory](#inventory-object) object.

## Park management

### `park/history`

Reports all park competitions the current user's pets have taken part in.

**Method**: `GET`

**Query string**:

* `filter[designGoal]` (optional): Fetch news articles tagged only
  with this numeric ID.
* `page` (optional): The number of the page in the news feed to fetch;
  zero by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  objects with the following structure:
  * `date`: A date string of the event.
  * `id`: An internal reference number for the event.
  * `participants`: An array of [short pet](#short-pet-object)
    objects, with the additional property `owner`, an object with the
    properties:
	* `icon`: A string denoting the owner's avatar icon.
	* `id`: The player's internal user ID.
	* `name`: The player's name.
  * `results`: A string containing the entire play-by-play of the
    competition.
  * `type`: The type of competition.

### `park/signUpPet/`*〈petID〉*

Signs a pet up for a specific park event.

**Method**: `POST`

**Payload**:

* `parkEventType` (optional): A string containing the event name
  ("Jousting", "Kin-Ball", or "Tri-D Chess") or `null`. If `null` or
  absent, withdraws the pet from competition.

**Response properties**: This API call returns no `data` property.

**Errors**:

* **"App\\Entity\\Pet object not found by the @ParamConverter
  annotation."**: received for a nonexistent pet ID.
* **"This is not your pet??? Reload and try again."**: received for a
  pet ID not belonging to the current user.
* **"*〈type〉* is not a valid park event type!"**: received if the
  event type selected is not one of the three recognized sports.

## Plaza activites

### `account/collectWeeklyCarePackage`

Used to collect a weekly care package from Tess in the plaza.

**Method**: `POST`

**Payload**:

* `type`: A number representing the box type to request: 4 is gaming.

**Response properties**:

* `data`: An [inventory](#inventory-object) object denoting the car
  package gained.

**Errors**:

* **"It's too early to collect your weekly Care Package."**: received
  if the player is not eligible for a care package right now.

### `recycling/gample`

Used to turn 100 recycling points into a roll of Magic Satyr Dice.

**Method**: `POST`

**Payload**:

* `bet`: A number, which is -1 for the "less than 8" bet (and probably
  0 and 1 for the equal and greater bets, respectively).

**Response properties**:

* `data`: An array of objects with the following properties:
  * `dice`: An array of two numbers indicating the rolled result.
  * `getDouble`: A boolean indicating whether the bet won for double payout.
  * `items`: An array of strings which are the names of the items acquired.
  * `points`: A number (TODO: figure out its purpose!)

**Errors**:

* **"You don't have enough !"**: received if the player does not have
  at least 100 recycling points.


## Combined reports

### `account/stats`

Used to get a hodge-podge of interesting stats about how often the
player has done various things.

**Method**: `GET`

**Response properties**:

* `data`: An array of objects with the following properties:
  * `firstTime`: A date string indicating when the recorded event
    first happened.
  * `lastTime`: A date string indicating when the recorded event most
    recently happened.
  * `stat`: A string describing the event being described.
  * `value`: A number indicating how many times the event has happened.

### `account/myHouse`

Used to populate the "House" screen in the web-client, and thus
providing a report on pets currently in the house and all items
currently in the house.

**Method**: `GET`

**Response properties**:

* `data`: An object with the two following properties:
  * `inventory`: An array of [inventory](#inventory-object) objects,
    containing inventory records for every item in your house.
  * `pets`: An array of [pet](#pet-object) objects, containing pet
    records for each pet in your house.

## Out-of-game info

### `article`

Gets postings to the news feed. This API call does not require an
authorization token.

**Method**: `GET`

**Query string**:

* `filter[designGoal]` (optional): Fetch news articles tagged only
  with this numeric ID.
* `page` (optional): The number of the page in the news feed to fetch;
  zero by default.

**Response properties**:

* `data`: A [list](#partial-list-object) whose results are
  [article](#article-object) objects.

### `article/latest`

Gets the most recent posting to the news feed. This API call does not
require an authorization token.

**Method**: `GET`

**Response properties**:

* `data`: An [article](#article-object) object. 

### `designGoal`

Gets all design goals. This API call does not require an authorization
token.

**Method**: `GET`

**Response properties**:

* `data`: An array of objects, each with the following properties:
  * `id`: The numeric identifier of the design goal.
  * `name`: The name of the design goal.
  * `description`: A description of what the design goal is.
* `user`, `weather`: As for every API call.

### `designGoal/`*〈designGoalID〉*

Gets the design goal with a specific ID number, included in
the API call. This API call does not require an authorization token.

**Method**: `GET`

**Response properties**:

* `data`: An object with the same properties as any element of the
  `data` property returned by the [`designGoal`](#designgoal) API call.

**Errors**:

* **"App\\Entity\\DesignGoal object not found by the @ParamConverter
  annotation."**: received for a nonexistent design goal.

### `deviceStats`

Used to tell Poppyseed Pets display-relevant device properties. For
any client which isn't laying out HTML pages which Poppyseed Pets
provides, there seems to be very little reason to use this API
call. The Javascript in the web client inherits all the payload
properties from the `navigator`, `screen`, and `window` Javascript
objects.

**Method**: `PUT`

**Payload**:

* `language`: String containing an ISO language code.
* `screenWidth`: Integer containing the width of the screen.
* `touchPoints`: Integer containing the maximum number of simultaneous
  touch points supported by the device.
* `userAgent`: String containing the User-Agent string of the browser.
* `windowWidth`: Integer containing the width of the window.

**Response properties**: This API call returns no `data` property.

# Commonly used objects

## Activity object

An activity is a report of an event which occurred; closely related to
an activity is a [pet log](#pet-log-object), as most activities describe
the same events that pet logs to, and they have many of the same
properties.

**Properties**:

* `changes`: `null` for activities which don't affect pet attributes,
  but othersise an object with the following propeties, indicating how
  the activity described affected certain pet attributes. Each
  property is either `null` to indicate no change, or a string
  containing one or more "+" or "-" characters showing increase or
  decrease.
  * `affection`: The affection a pet has for its owner.
  * `affectionLevel`: The number of times affection increases have granted
    a pet a chance to get a merit.
  * `esteem`: The extent of a pet's self-esteem.
  * `exp`: A pet's experience point count.  
  * `food`: The extent of a pet's satiation.
  * `level`: A pet's experience level.  
  * `love`: The extent to which a pet feels loved.
  * `safety`: The extent to which a pet feels safe.
  * `scrollLevel`: Not sure what this is. (TODO: Find out!)
* `createdOn`: A date string giving when the event occurred.
* `entry`: A string of the text describing the event.
* `icon`: A string of text giving the location of the icon appearing before the
  event in the activity log.
* `interestingness`: A number, usually zero? Not sure what it signifies.
* `isPetActivity`: A boolean indicating whether this activity is the
  result of something a pet did autonomously (as opposed to, say, a
  market purchase).
* `tags`: An array containing zero or more objects, each with the
  following properties describing tags of the activity:
  * `title`: A string describing the tag.
  * `color`: A color string denoting the background color of the tag.
  * `emoji`: A unicode string with emoji appearing alongside the tag's title.

## Article object

A data structure describing a news article.

**Properties**:

* `author`: An object containing the two following subproperties:
  * `id`: An ID number denoting the author (pretty much always 1).
  * `name`: A string describing the author (pretty much always "Ben").
* `body`: A string which is the body text of the news item, in HTML form.
* `createdOn`: A date string indicating when the news item was created.
* `designGoals`: An array of objects, each of which corresponds to a
  design goal, and which has the following properties:
  * `id`: An ID number denoting the design goal.
  * `name`: A string describing the design goal.
* `id`: A number representing the article; articles appear to be
  sequentially numbered as a rule.
* `title`: A string which is the subject line of the news item.

## Enchantment object

An enchantment data structure is used to describe an enchantment
which can be or has been appled to a tool.

**Properties**:

* `aura` (optional): `null` for enchantments which do not impart an
  aura; otherwise an object which provides some combination of the
  following properties:
  * `name`: The name of an aura which has no actual effect (TODO:
    figure out what's going on with these)
  * `id`: A number denoting the aura effect in some internal database.
  * `image`: A string denoting the image used for the aura.
  * `size`: A number indicating the size of the aura-image.
  * `centerX`: A number indicating the horizontal position of the
    aura-image's center.
  * `centerY`: A number indicating the vertical position of the
    aura-image's center.
* `effects`: A [tool](#tool-object) object indicating additional
  tool effects imparted by the enchantment. The various `grip`
  attributes are always zero, as far as I can tell.
* `id`: A number representing the enchantment's internal ID number.
* `isSuffix`: A boolean indicating whether the enchantment name
  should follow the item name.
* `name`: A string used to denote the enchantment in an item's display name

## Food object

A data structure describing the food qualities of an edible item, or
additional food qualities imparted by a spice.

**Properties**:

* `bringsLuck`: `null` or a string naming the type of luck which is
  imparted by eating the food ("Good Luck" or "Fish Bones").
* `candy`: A boolean indicating whether the food is considered candy
  for Halloween (and possibly other) purposes.
* `grantedStatusEffect`: null or a string naming a status effect
  imparted by the food. Note that some status effects are also
  imparted by modifiers, and are not named in this property, e.g. the
  "Hallucinating" effect is caused by toadstools, which have the
  modifier "trippy", but do not have "Hallucinating" as a
  grantedStatusEffect.
* `grantedSkill`: null or a string naming a skill which will be
  increased if it is zero.
* `leftovers`: `null` or a string naming an item which remain after the food is consumed.
* `modifiers`: An array of strings describing the food's healthiness,
  size, trippiness, flavors, etc.

## Group object

A data structure describing a group of pets.

**Properties**:
* `createdOn`: A date string indicating the date of the group's
  formation.
* `id`: A number representing the group internally.
* `lastMetOn`: A date string indicating the date of the group's
  last meeting.
* `makesStuff`: A boolean indicating whether the group makes stuff
  (gaming groups do not).
* `members`: An array of [pet](#pet-object) objects.
* `name`: A string representing the group's world-readable name.
* `numberOfProducts`: A number representing the number of times the
  group has produced.
* `progress`: A number (presumably between 0 and 100?)) indicating
  percentage of progress towards a product. This field is *only*
  reported by the [`pet/`*〈petID〉*`/friends`](#petpetidfriends)
  API call.
* `type`: A number encoding the type of group (1="Band", 2="Astronomy
  Lab", 3="Gaming Group", 4="Sportsball Team").


### Short group object

In a [`pet/`*〈petpetid〉*`/friends`](#petpetidfriends) call, only
`id`, `makesStuff`, `name`, `progress`, and `type` are populated.

## Guild object

A data structure describing a pet guild.

**Properties**:
* `emblem`: A string indicating the location of an image
  representing the guild.
* `id`: A number representing the guild internally.
* `name`: The human-readable name for the guild.
* `quote`: A string which is descriptive of the guild somehow? It's
  empty in all of the current guilds.
* `starterTool`: An object indicating the favored item of the guild,
  with the following properties (which are equal to the same
  properties in the appropriate [item](#item-object) object.
  * `name`: The item's common name.
  * `image`: A string denoting the source location of graphics for
    this item.

### Short guild object

Most uses of a guild object in the API leave out the `quote` and
`starterTool` properties.

## Inventory object

An inventory data structure is used to describe a specific instance of
an item. It includes the entirety of the [item](#item-object)'s own data
in one of its properties, but also includes details specific to an
individual instance of that item (spice, enchantment, creator, etc.)

**Properties**:

* `comments`: An array of strings which provide inventory-specific
  descriptions, e.g. how and by whom the inventory item was created.
* `createdBy`: `null` if not created by a player, but otherwise an
  object with the following properties:
  * `id`: A number representing the creating player's ID number.
  * `name`: A string of the creating player's name.
* `createdOn`: A date string indicating when this item was created.
* `enchantment`: `null` for unenchanted items, otherwise an
  [enchantment](#anchantment-object) object denoting the enchantment
  the item has.
* `holder`: `null` for non-tools and unequipped tools; otherwise an
  object with the following properties:
  * `id`: Number representing the ID of the pet holding it.
  * `name`: String containing the name of the pet holding it.
* `id` : A number representing the inventory item's internal ID
  number. Note this is different from the underlying generic-item's ID
  number, which appears in the property `item.id`.
* `item`: An [item](#item-object) object describing the generic
  properties of this object.
* `lockedToOwner`: A boolean indicating whether the item can be sold.
* `modifiedOn`: A date string representing the last time
  something-or-other was done to this item; for instance, for a
  jukebox, it updates whenever the jukebox is listened to.
* `sellPrice`: `null` if the inventory item is not for sale; otherwise
  the price for which it is being sold in the market.
* `spice`: `null` if this item has no spice, otherwise an object with
  the following properties:
  * `effects`: A [food](#food-object) object indicating additional food
    effects caused by the spice.
  * `isSuffix`: A boolean indicating whether the spice name should
    follow the item name.
  * `name`: A string used to denote the spice in an item's display name.
* `wearer`: `null` for unworn items; otherwise an object with the
  following properties:
  * `id`: Number representing the ID of the pet wearing it.
  * `name`: String containing the name of the pet wearing it.

### Short inventory object

Some API calls return an abbreviated version of an inventory object,
with a limited set of fields. These objects have only the
`enchantment`, `id`, `item`, `sellPrice` and `spice` fields, but each
of those fields has the same meaning it would in a full
[inventory](#inventory-object) object.


## Item object

An item data structure is used to describe a class of object which
players can possess. Individual instances which are possessed by
players are actually represented by [inventory](#inventory-object)
objects, but an item data structure describes properties of a type of
possession, rather than the properties of specific instances of that
item.

**Properties**:

* `description`: `null` or a string describing an object.
* `enchants`: `null` for items which do not confer a tool bonus;
  otherwise an [enchantment](#enchantment-object) object denoting the
  tool bonus it provides.
* `food`: `null` for non-edibles; for edible objects, a
  [food](#food-object) object describing the food properties of this
  item.
* `greenhouseType`: `null` for non-plantables, a string describing a
  greenhouse zone for plantable objects.
* `hat`: `null` for non-hats; for hats, an object describing the
  geometry of where it sits on a pet's head, with the following
  properties:
  * `headAngle`: A number representing the angle at which it sits on the
    pet's head.
  * `headAngleFixed`: A boolean determining whether the angle of the hat
    should be unadjusted for the pet's head position (e.g. true for
    balloons, false for more conventional hats)
  * `headScale`: A number representing the size of the object when
    depicted as a hat.
  * `headX`: A number representing the X-position of the object when
    depicted as a hat.
  * `headY`: A number representing the X-position of the object when
    depicted as a hat.
* `hollowEarthTileCard`: `null` for non-tiles, for tiles, an object with
  the following properties:
  * `name`: A string representing the name of the tile itself.
  * `type`: An object with the properties:
    * `name`: The name of the section of the hollow earth where it can appear.
    * `article`: The article of the section of the hollow earth where
      it can appear.
* `id`: A number representing the internal ID of this object class.
* `image`: A string denoting the source location of graphics for this
  object.
* `isFertilizer`: A boolean representing whether this object can be
  used as fertilizer.
* `isFlammable`: A boolean representing whether this object can be
  burned in a fireplace.
* `name`: A string representing the user-visible name of this item.
* `recycleValue`: A number representing the quantity of recycle-points
  provided by recycling this item.
* `spice`: `null` if this item is not a spice, otherwise an object with
  the following properties:
  * `effects`: A [food](#food-object) object indicating additional food
    effects this spice will cause.
  * `isSuffix`: A boolean indicating whether the spice name should
    follow the item name.
  * `name`: A string used to denote the spice in an item's display name.
* `tool`: `null` for non-tools; for tools, a [tool](#tool-object) object
  describing tool effects imparted by the enchantment. The various
  `grip` attributes are always zero, as far as I can tell.
* `useActions`: An array of arrays; each element of this array is a
  two- or three-element array of strings. The first string in this
  array is the text appearing on a user-visible button to take an
  action; the second string is the API call to invoke this action. The
  wildcard "#" in this API call is expanded as the inventory ID of a
  specific inventory object to invoke the action on; see the API call
  [`item/`*〈itemtype〉*`/`*〈inventoryID〉*`/`*〈action〉*](#itemitemtypeinventoryidaction)
  for more information. The third element of a `useActions` entry is
  rare and seems to indicate when an action is not simply a simple
  one-step process: for instance, the cooking buddy's use action has
  "page" as its third string.

## Partial list object

Whenever Poppyseed Pets is producing a single page out of a search, it
will produce a data structure which describes the search as a whole,
the single page being asked for, and a list of the individual objects
appearing on that page; depending what kind of search is being done,
this could be a list of news articles, players, pets, items,
etc. but the same underlying structure is used for all these types of
lists.

**Properties**:

* `page`: The number of the page being delivered in this partial list.
* `pageCount`: The total number of pages in the search.
* `pageSize`: The number of individual objects per page.
* `resultCount`: The total number of individual objects in the whole
  search. As a general rule, `pageCount` should always be
  `resultCount` divided by `pageSize` and rounded up.
* `results`: An array of individual objects on this page. Usually there
  will be `pageSize` objects, but the final page of a search will have
  fewer.
* `unfilteredTotal`: The total number of objects among which the search
  was performed.

## Pet object

A pet data structure is used to describe a pet. Several properties of
the pet object are *private* and will not be provided on API calls
which return other people's pets' data. Private properties are
identified as such below.

**Properties**:

* `affectionLevel` (private): Number containing the number of times
  affection increases have granted this pet a chance to get a merit.
* `affectionRewardsClaimed` (private): Number containing the number of
  merits gained as a result of affection increases.
* `alcoholLevel` (private): A string representing the pet's level of
  inebriation (TODO: list valid values)
* `birthDate`: A date string indicating when the pet was born.
* `canInteract` (private): A boolean representing whether the pet can
  be petted at present.
* `canParticipateInParkEvents` (private): A boolean representing
  whether the pet is able to participate in park events (it's false
  before you get park access, maybe?)
* `canPickTalent` (private): `null` if the pet is not currently in a
  position to learn a talent. "expertise" when the talent options are
  Force of Will, Force of Nature, and Balance. (TODO: Maybe others?)
* `colorA`: A color string denoting the pet's first color.
* `colorB`: A color string denoting the pet's second color.
* `costume`: A string, empty when it's not Halloween (TODO: see what's
  there on Halloween).
* `craving` (private): `null` when a pet has no craving, and a string
  representing the craving when they have one.
* `esteemed` (private): A string representing the pet's esteem level
  (from lowest to highest seen: "useless", "...", "accomplished",
  "amazing") (TODO: determine levels I haven't seen).
* `flavor` (private): A string representing the pet's preferred
  flavor.
* `full` (private): A string representing the pet's food level (from
  lowest to highest seen: "starving", "very hungry", "hungry", "...",
  "sated", "full", "stuffed") (TODO: determine levels I haven't seen).
* `groups`: An array of [short group](#short-group-object) objects of
  which this pet is a member.
* `guildMembership`: An object with the following properties.
    * `guild`: An object with the following properies.
      * `emblem`: A string indicating the location of an image
        representing the guild.
      * `id`: A number representing the guild internally.
      * `name`: The human-readable name for the guild.
    * `joinedOn`: A date string indicating when the pet joined the guild.
    * `rank`: A string describing the pet's rank in the guild.
* `hallucinationLevel` (private): A string representing the pet's
  level of hallucination (from lowest to highest seen: "none",
  "low"). (TODO: find other valid values)
* `hasRelationship` (private): A boolean representing whether the pet
  has relationships with other pets.
* `hat`: An object which describes a pet's hat with a [short
  inventory](#short-inventory-object) object.
* `id`: A number representing the internal ID number of this pet,
  typically used to refer to it in API calls.
* `isFertile` (private): A boolean indicating whether the pet can
  become pregnant/eggnant.
* `lastParkEvent` (private): A date string indicating when the pet
  last competed in a park event.
* `level`: A number representing the pet's experience level.
* `location` (optional): A string representing the pet's current
  location; possibilities include "home", "daycare", "dragon den",
  "greenhouse", and possibly others.
* `lunchboxItems` (private): An array of [short
  inventory](#short-inventory-object) objects which are in this pet's
  lunchbox.
* `maximumFriends`: A number indicating this pet's maximum number of friends.
* `merits`: An array of objects, each of which contains only the
  property `name` describing a merit possessed by the pet.
* `name`: A string containing the pet's name.
* `note` (private): A string containing the pet's user-inputed notes.
* `parkEventType` (private): A string containing the name of the pet's
  chosen park event; `null` if the pet is not signed up for any event.
* `poisonLevel` (private): A string representing the pet's level of
  poisoning (from lowest to highest seen: "none", "a little") (TODO:
  find other valid values)
* `pregnancy`: `null` for nonpregnant pets; for pregnant pets an
  object with the following properties:
  * `eggColor`: a color string indicating the egg color.
  * `pregnancyProgress`: a string whose text describes the current
    pregnancy status.
* `safe` (private): A string representing the pet's safety level (from
  lowest to highest seen: "terrified", "on edge", "...", "safe",
  "invincible") (TODO: determine levels I haven't seen).
* `scale`: A number representing the pet's size (for graphic
  presentation purposes)
* `selfReflectionPoint`: A number whose meaning I don't yet
  know. (TODO: figure it out)
* `skills` (private): An object whose properties correspond to pet
  skills, with the names `brawl`, `canSeeInTheDark`, `climbingBonus`,
  `crafts`, `dexterity`, `fishingBonus`, `gatheringBonus`,
  `hasProtectionFromHeat`, `intelligence`, `music`, `nature`,
  `perception`, `science`, `smithingBonus`, `stamina`, `stealth`,
  `strength`, and `umbra`. Each of these properties is an object with
  the following properties:
  * `base`: A number representing the pet's intrinsic ability.
  * `merits`: A number representing the ability conferred by merits.
  * `tool`: A number representing the ability conferred by tools.
  * `statusEffects`: A number representing the ability conferred by
    current status effects.
* `species`: A [species](#species-object) object denoting the pet's species.
* `spiritCompanion`: `null` for pets without a spirit companion, and
  for those with a spirit companion, an object with the following
  properties:
  * `image`: A string denoting the source location of graphics for the
    spirit companion.
  * `name`: A string of the spirit companion's name.
  * `star`: A string of the star the spirit companion is from.
* `statuses` (private): An array of strings which are the names of
  statuses.
* `tool`: An object which describes a pet's tool with a [short
  inventory](#short-inventory-object) object.

### Short pet object

Some large data structures, when referring to a pet, return an object
with only the necessary details to properly render the pet, which are
`colorA`, `colorB`, `hat`, `id`, `merits`, `name`, `pregnancy`,
`scale`, `species`, `spiritCompanion`, and `tool`.

## Pet log object

A pet log is very similar to an [activity](#activity-object) but
describes specifically activities undertaken by pets and has a
slightly different structure.

**Properties**:

* `changes`: Same as in an [activity](#activity-object) object.
* `createdOn`: Same as in an [activity](#activity-object) object.
* `entry`: Same as in an [activity](#activity-object) object.
* `equippedItem`: An object briefly describing the pet's equipped
  item, with two properties:
  * `image`: A string giving the location of a picture.
  * `name`: A string giving the name of the item.
* `icon`: Same as in an [activity](#activity-object) object.
* `interestingness`: Same as in an [activity](#activity-object) object.
* `pet`: An object which briefly describes the pet who undertook the
  activity (not a full [pet](#pet-object) object), with the following
  properties:
  * `colorA`: As in a [pet](#pet-object) object.
  * `colorB`: As in a [pet](#pet-object) object.
  * `id`: As in a [pet](#pet-object) object.
  * `merits`: As in a [pet](#pet-object) object.
  * `name`: As in a [pet](#pet-object) object.
* `tags`: Same as in an [activity](#activity-object) object.

## Relationship object

A data structure describing a pet's relationship to another pet.

**Properties**:
* `committment`: A number representing the strength of the relationship.
* `currentRelationship`: A string describing the relationship type;
  "bff", "mate", "fwb", "friend", "dislike", and "friendly rival" are
  all possibilities.
* `lastMet`: A date string indicating when the pets last interacted.
* `metDescription`: A string indicating the circumstances of their
  first meeting.
* `metOn`: A date string indicating when the pets first interacted.
* `relationshipWanted`: A string of the same form as
  `currentRelationship` for pets with the "Introspective" merit; null
  otherwise.


## Species object

A data structure describing pet species and relevant geometry.

**Properties**:

* `availableAtSignup`: A boolean denoting whether the species can be
  an initial pet.
* `availableFromPetShelter`: A boolean denoting whether the species
  can be be born from breeding.
* `availableFromPetShelter`: A boolean denoting whether the species
  can be found in the pet shelter.
* `eggImage`: `null` for non-egg-laying species; a string denoting an
  image location for egg-laying species.
* `family`: A string denoting the family the species belongs to
  (including "bird", "elemental", "fish", "mammal", "monotreme", and
  many others).
* `flipX`: A boolean denoting whether the pet should be flipped left-to-right.
* `handAngle`: A number indicating the angle to be imparted to tools
  without the `gripAngleFixed` property set.
* `handBehind`: A boolean used to determine how tools are layered with
  the pet (I think?).
* `handX`: A number indicating the x-coordinate shift used to position
  tools wielded by pets of this species.
* `handY`: A number indicating the y-coordinate shift used to position
  tools wielded by pets of this species.
* `hatAngle`: A number indicating the angle to be imparted to hats
  without the `hatAngleFixed` property set.
* `hatX`: A number indicating the x-coordinate shift used to position
  hats worn by pets of this species.
* `hatY`: A number indicating the y-coordinate shift used to position
  hats worn by pets of this species.
* `id`: A number representing the species's internal number (probably
  chronological by creation date).
* `image`: a string denoting an image location for the species' picture.
* `name`: a string denoting the species' name.
* `numberOfPets`: A number indicating how many pets in the game have
  this species.
* `pregnancyStyle`: a number denoting how the species reproduces, with
  0 for egg-laying, 1 for live-birth.

### Short species object

In several contexts only subsets of the species properties show
up. Poppyopedia entries lack most of the geometry properies, while
pets' `species` properties lack the `availableAtSignup`,
`availableFromBreeding`, `id`, and `numberOfPets` subproperties.

## Theme object

A theme data structure consists mostly of colors for various onscreen
elements.

**Properties**:
* `backgroundColor`: A color string denoting the main background color.
* `bonusAndSpiceColor`: A color string denoting the text color for
  tool bonuses and spices.
* `bonusAndSpiceSelectedColor`: A color string denoting the text color
  for tool bonuses and spices on a selected object.
* `buttonTextColor`: A color string denoting the text color for text
  on buttons.
* `dialogLinkColor`: A color string denoting the text color for links
  appearing in speech bubbles.
* `gainColor`: A color string denoting the text color for text
  corresponding to successes.
* `id`: An integer corresponding to some internal index of themes.
* `inputBackgroundColor`: A color string denoting the color of
  text-input fields.
* `inputTextColor`: A color string denoting the color of text written
  in text-input fields.
* `linkAndButtonColor`: A color string denoting the color of buttons
  and in-text links.
* `name`: The name attached to the theme.
* `petInfoBackgroundColor`: A color string denoting the color of the
  background of each pet's info field.
* `primaryColor`: A color string denoting the color of the header bar
  and the background of selected items.
* `speechBubbleBackgroundColor`: A color string denoting the color of
  speech bubbles.
* `tabBarBackgroundColor`: A color string denoting the background
  color of the tab bar and NPC names.
* `textColor`: A color string denoting the color of all text which
  doesn't have some special coloration described elsewhere.
* `textOnPrimaryColor`: A color string denoting the color of text on
  the header bar and selected items.
* `textColor`: A color string denoting the color of all warning text
  (not sure where this is used, exactly).

## Tool object

A data structure describing the tool qualities of a wieldable item, or
additional tool qualities imparted by an enchantment.

**Properties**:

* `alwaysInFront`: A boolean indicating whether the tool can be
  occluded by the pet's body.
* `gripAngle`: A number representing the angle at the object is
  gripped.
* `gripAngleFixed`: A boolean determining whether the angle of the
    grip should be unadjusted for the pet's hand position (e.g. true
    for things that hang or float, false for rigid tools)
* `gripScale`: A number representing the size of the object when
    depicted as a tool.
* `gripX`: A number representing the X-position of the object when
    depicted as a tool.
* `gripY`: A number representing the X-position of the object when
    depicted as a tool.
* `modifiers`: An array of strings describing the effects the tool has.

### Short tool object

Sometimes, a tool object (particularly in the poppyopedia or market?)
has only the `modifiers` property instead of any of the geometry
details.

## User object

A user data structure, typically included as the `user` property in a
JSON block, describes many of the details of a user's character. Pets
and possessions are in separate structures, but intrinsics to the user
are in this object. All the properties of this object except for
`icon`, `id`, `lastActivity`, `name`, and `registeredOn` are *private*
and will be left out of the `data` property of various `account` API
calls (which are used to query other people's accounts), but all of
the properties appear in the `user` property of a standard API call
(which reports information on the logged-in user's own account).

**Properties**:
* `canAssignHelpers`: A boolean identifying whether the player has the
  power to assign pets to tasks (not sure when/why it would be false).
* `defaultSessionLengthInHours`: A number representing how long a
  session's token last by default.
* `email`: A string of the user's email address.
* `icon`: `null` if the user has no profile avatar; otherwise a string
  denoting the location of the avatar.
* `id`: A number representing the user's internal ID.
* `lastActivity`: A date string representing the last time the user
  did anything (not included on every-API-call `user` field).
* `lastAllowanceCollected`: A date string representing the last time
  the user collected an "allowance" from Tess (I think? It's set to
  midnight, and on a different day than the care package was collected).
* `maxMarketBids`: A number representing the maximum number of bids
  the player can have active on the market.
* `maxPets`: A number representing the maximum number of pets
  the player can have in their house.
* `maxPlants`: A number representing the maximum number of plants
  the player can have in their greenhouse.
* `maxSellPrice`: A number representing the highest sale price this
  player is allowed.
* `menu`: An array of the menu items available to the player, each of
  which is an object with the following three properties.
  * `location`: A string describing the menu item, e.g. "home",
    "petShelter", "florist".
  * `isNew`: A boolean indicating whether the location gets a 'new'
    banner on the menu.
  * `sortOrder`: A number indicating the position where the location
    is displayed on the menu.
* `moneys`: A number representing the player's wealth.
* `name`: A string representing the player's name.
* `recyclePoints`: A number representing the player's recycle credit.
* `roles`: An array of strings; it seems to be just the single string
  "ROLE_USER" for an ordinary user; likely Ben has something special
  instead/in addition.
* `unlockedBasement`: A date string indicating when the user got
  a basement or `null` if they haven't yet.
* `unlockedBeehive`: A date string indicating when the user got a
  beehive or `null` if they haven't yet.
* `unlockedBookstore`: A date string indicating when the user got
  access to the bookstore or `null` if they haven't yet.
* `unlockedBulkSelling`: A date string indicating when the user unlocked
  bulk-selling capabilities or `null` if they haven't yet.
* `unlockedDragonDen`: A date string indicating when the user got a
  dragon den or `null` if they haven't yet.
* `unlockedFieldGuide`: A date string indicating when the user got
  access to the field guide or `null` if they haven't yet.
* `unlockedFireplace`: A date string indicating when the user got a
  fireplace or `null` if they haven't yet.
* `unlockedFlorist`: A date string indicating when the user got access
  to the florist or `null` if they haven't yet.
* `unlockedGreenhouse`: A date string indicating when the user got a
  greenhouse or `null` if they haven't yet.
* `unlockedHattier`: A date string indicating when the user got access
  to the hattier or `null` if they haven't yet.
* `unlockedHollowEarth`: A date string indicating when the user got a
  portal to the Hollow Earth or `null` if they haven't yet.
* `unlockedMailbox`: A date string indicating when the user got a
  mailbox or `null` if they haven't yet.
* `unlockedMarket`: A date string indicating when the user got access
  to the market or `null` if they haven't yet.
* `unlockedMarket`: A date string indicating when the user got access
  to the museum or `null` if they haven't yet.
* `unlockedPark`: A date string indicating when the user got access
  to the park or `null` if they haven't yet.
* `unlockedRecycling`: A date string indicating when the user got the
  ability to recycle or `null` if they haven't yet.
* `unlockedTrader`: A date string indicating when the user got access
  to the trader or `null` if they haven't yet.
* `unreadLetters`: A number of unread mailbox messages.
* `unreadNews`: A number of unread news articles.


## Weather object

The weather data structure includes current weather and the next 24
hours worth of weather forecasts in game.

**Properties**:

* `forecast`: An array of 24 objects representing the next 24 hours of
  weather, all of which have the same form as the `today` property
  described below.
* `today`: An object describing the current weather, with the
  following properties:
  * `clouds`: A number (between 0 and 1?) representing the quantity of
    cloud cover.
  * `holidays`: An array of strings describing holidays, e.g. "Flower Moon".
  * `isNight`: A boolean representing whether it is night.
  * `rainfall`: A number (between 0 and 1?) representing the rain level.
  * `temperature`: A number containing current temperature Centigrade.
